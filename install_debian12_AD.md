# Installation de Debian 12 sur une AD

## Install logiciels

```bash
apt -y install realmd libnss-sss libpam-sss sssd sssd-tools adcli samba-common-bin oddjob oddjob-mkhomedir packagekit
```

Détection du domaine

```bash
realm discover TLPU067.etab.local
```

joindre le domaine

```bash
realm join -U administrateur TLPU067.etab.local
```

le compte peut être le compte administrateur ou tout autre compte qui a les droits pour joindre 
une machine au domaine.

éditer le fichier `/etc/pam.d/common-session` pour créer le home_dir :

```bash
session optional        pam_mkhomedir.so skel=/etc/skel umask=077
```

éditer le fichier /etc/sssd/sssd.conf et mettre à False le nom complet

```apacheconf
use_fully_qualified_names = False
```

Pour permettre la connexion sssd, il faut ajouter à la fin du fichier sous la partie active directory

```apacheconf
ad_gpo_ignore_unreadable = True
ad_gpo_access_control = permissive
```

Montage des disques

libpam mount

```bash
apt-get install libpam-mount cifs-utils
#creation du dossier pour le point de montage partage
mkdir /mnt/Partage
chmod 777 /mnt/Partage
```
